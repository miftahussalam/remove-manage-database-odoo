{
    "name"          : "Web",
    "version"       : "1.0",
    "author"        : "Miftahussalam",
    "website"       : "https://miftahsalam.wordpress.com",
    "category"      : "Custom Module",
    "description"   : """
        Remove Link Database Manager, Powered by Odoo, etc.
    """,
    "depends"       : [
        "base",
        "web",
    ],
    "init_xml"      : [],
    "demo_xml"      : [],
    "data"          : [
        'views/ms_webclient_templates.xml',
    ],
    "js"            : [],
    "css"           : [],
    "active"        : False,
    "application"   : True,
    "installable"   : True
}